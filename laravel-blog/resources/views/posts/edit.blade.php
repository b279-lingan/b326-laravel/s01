@extends('layouts.app')

@section('tabName')
    Edit Post
@endsection

@section('content')
    <form class="col-10" action="{{ route('post.update', ['id' => $post->id]) }}" method="POST">
            @csrf
            @method('PUT')
            <div class = "form-group">
            <h5>Title:</h5>
            <input type="text" name="title" value="{{ $post->title }}" class="form-control">
            </div>

            <div class = "form-group">
            <h5>Content:</h5>
            <textarea name="content" class = "form-control">{{ $post->content }}</textarea>
            </div>

            <div class = "mt-2">
            <button type="submit" class="btn btn-primary">Update Post</button>
            </div>
        </form>
@endsection
