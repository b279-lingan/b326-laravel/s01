@extends('layouts.app')

@section('tabName')
    Post Creations
@endsection

@section('content')
    <form class = "col-3 bg-secondary p-5 mx-auto" method = "POST" action = '/posts'>
        <!-- CSRF stands for Cross-Site Request Forgery. It is form of attact where malicious users may send malicius request while pretending to be authorized user. Laravel uses token detect if form input request have not -->

        @csrf
        <div class="form-group">
            <label for='title'>Title:</label>
            <input type="text" name="title" class="form-control" id="title"/>
        </div>

        <div class="form-group">
            <label for ="content">Content:</label>
            <textarea class="form-control" id="content" name="content" rows=3></textarea>
        </div>

        <div class = "mt-2">
            <button class="btn btn-primary">Create Post</button>
        </div>
    </form>
@endsection
